<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'rest';

// generate token
$route['reGenToken'] = 'api/Token/reGenToken';

// load all clients
$route['api/clients/allClients'] = 'api/clients/allClients';
// insert client
$route['api/clients/insertClient'] = 'api/clients/insertClient';

// load all credits
$route['api/credits/allCredits'] = 'api/credits/allCredits';
// find credit by $id
$route['api/credits/findCreditById'] = 'api/credits/findCreditById';
// find credit by $phone
$route['api/credits/findCreditByPhone'] = 'api/credits/findCreditByPhone';
// insert credit
$route['api/credits/insertCredit'] = 'api/credits/insertCredit';

// load logs
$route['api/logs/all'] = 'api/logs/all';
// insert client
$route['api/logs/insertLog'] = 'api/logs/insertLog';

// all products
$route['api/products/allProducts'] = 'api/products/allProducts';
// search product by $code
$route['api/products/findProduct'] = 'api/products/findProduct';
// insert product
$route['api/products/insertProduct'] = 'api/products/insertProduct';

// load all users
$route['api/users/allUsers'] = 'api/users/allUsers';
// search user by $username & $password
$route['api/users/findUser'] = 'api/users/findUser';
// insert user
$route['api/users/insertUser'] = 'api/users/insertUser';

// test token api
$route['api/test/token'] = 'api/test_api/tokenGen';
// test verify token api
$route['api/test/verify/token'] = 'api/test_api/verify';
// test login api
$route['api/test/login'] = 'api/test_api/login';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
