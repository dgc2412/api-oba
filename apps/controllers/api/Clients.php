<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include Rest Controller library 
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Clients extends REST_Controller {

	/**
	 * Api clients controller.
	*/

    function __construct() {
        // Constructeur de la classe
        parent::__construct();

        // chargement du model associe
        $this->load->model('Clients/clients_model');
        
        // definition des méthodes
        $this->methods['allClients_get']['limit'] = 100;
        $this->methods['insertClient_post']['limit'] = 20;
    }

    public function allClients_get() {
        try {
            // save headers
            // $headers = $this->input->request_headers(); 
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $clients = $this->clients_model->getAllClients();

                    (!empty($clients))?$this->response(['status' => TRUE, 'msg' => 'OK', 'res' => $clients], REST_Controller::HTTP_OK):
                    $this->response(['status' => FALSE, 'msg' => 'Aucun client trouve.', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function insertClient_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $phone = $this->post('phone');
                    // verify if is number
                    if(!empty($phone) && preg_match('/^[0-9]{10}+$/', $phone)){
                        //  verify if number is orange
                        if(substr($phone, 0, 2) === "07") {
                            $client = $this->existClient($phone);
            
                            if(!empty($client)) $this->response(['status' => TRUE, 'msg' => 'Client existe deja !', 'res' => $client], REST_Controller::HTTP_OK);
                            else {
                                $id = $this->clients_model->insertClient($phone);
                                $this->response(
                                    ['status' => TRUE, 'msg' => 'OK', 'res' => array($id)], REST_Controller::HTTP_CREATED
                                );
                            }
                        } else {
                            $this->set_response(['status' => FALSE, 'msg' => 'Vous devez saisir un numero orange !'], REST_Controller::HTTP_BAD_REQUEST);
                        }
                    } else {
                        $this->set_response(['status' => FALSE, 'msg' => 'Numero incorrecte !'], REST_Controller::HTTP_BAD_REQUEST);
                    }
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function existClient($phone) {
        $client = $this->clients_model->getAllClients($phone);

        return $client;
    }

}