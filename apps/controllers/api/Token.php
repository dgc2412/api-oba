<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends REST_Controller
 */
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Token extends REST_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	*/
	public function __construct() {
		parent::__construct();
        $this->load->library('Authorization_Token');

		// appname
		$this->appname = "obatest";

        $this->methods['reGenToken_post']['limit'] = 20;
	}

	/**
	 * register function.
	 * 
	 * @access public
	 * @return void
	 */
    
	public function reGenToken_post() {

		// set variables from the form
		$appname = $this->input->post('appname');
		if(isset($appname)) {
			
			if ($appname === $this->appname) {

				// token regeneration process
				$token_data['uid'] = $this->generateUID(6);
				$token_data['appname'] = $appname; 
				$tokenData = $this->authorization_token->generateToken($token_data);
				// response to send
				$final = array();
				$final['access_token'] = $tokenData;
				$final['status'] = true;

				$this->response($final, REST_Controller::HTTP_OK); 
			} else $this->response(['appname invalide'], REST_Controller::HTTP_NOT_FOUND);
		} else $this->response(["msg" => "appname est necessaire", "appname" => $appname], REST_Controller::HTTP_BAD_REQUEST);

	}

	private function generateUID($n) {
      
        // Take a generator string which consist
        $generator = "A1B3C5D7E9F0G2H4I6J8KMLNPOQTSVUXYWZ";
      
        $result = "";
      
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
      
        // Return result
        return $result;
    }
}