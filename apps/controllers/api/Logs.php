<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include Rest Controller library 
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Logs extends REST_Controller {

	/**
	 * Api logs controller.
	*/

    function __construct()
    {
        // Constructeur de la classe
        parent::__construct(); 
        
        // chargement du model associe
        $this->load->model('Logs/logs_model');

        // definition des méthodes
        $this->methods['all_get']['limit'] = 100;
        $this->methods['insertLog_post']['limit'] = 20;
    }

    public function all_get() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $log = $this->logs_model->getAll();

                    (!empty($log))?$this->response(['status' => TRUE, 'msg' => 'OK', 'res' => $log], REST_Controller::HTTP_OK):
                    $this->response(['status' => FALSE, 'msg' => 'Aucune action', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function insertLog_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $idUser = $this->post('idUser');
                    $action  = $this->post('action');
                    
                    if(!empty($idUser) && !empty($action)) {
                        $this->logs_model->insertLog($idUser, $action);
                        $this->response(
                            ['status' => TRUE, 'msg' => 'Action ajoutee !', 'res' => array()], REST_Controller::HTTP_CREATED
                        );
                    } else $this->set_response(['status' => FALSE, 'msg' => 'Aucune valeur ne doit etre vide !'], REST_Controller::HTTP_BAD_REQUEST);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}