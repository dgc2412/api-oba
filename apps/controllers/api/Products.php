<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include Rest Controller library 
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Products extends REST_Controller {

	/**
	 * Api product controller.
	*/

    function __construct()
    {
        // Constructeur de la classe
        parent::__construct(); 
        
        // chargement du model associe
        $this->load->model('Products/products_model');

        // definition des méthodes
        $this->methods['allProducts_get']['limit'] = 100;
        $this->methods['findProduct_post']['limit'] = 20;
        $this->methods['insertProduct_post']['limit'] = 20;
    }

    public function allProducts_get() {
        try {
            // save headers
            $headers = $this->input->request_headers(); 
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $products = $this->products_model->getAllProducts();

                    (!empty($products))?$this->response(['status' => TRUE, 'msg' => 'OK', 'res' => $products], REST_Controller::HTTP_OK):
                    $this->response(['status' => FALSE, 'msg' => 'Aucun produit trouve.', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function findProduct_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $code = $this->post('code');
                    if(!empty($code)) {
                        $product = $this->products_model->findProduct($code);

                        if(!empty($product)) $this->response(['status' => TRUE, 'msg' => 'Produit existe !', 'res' => $product], REST_Controller::HTTP_OK);
                        else $this->response(['status' => FALSE, 'msg' => 'Aucun produit correspondant!', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
                    } else $this->set_response(['status' => FALSE, 'msg' => 'Aucune valeur ne doit etre vide !'], REST_Controller::HTTP_BAD_REQUEST);                   
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function insertProduct_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    // variables
                    $code = $this->post('code');
                    $minAmount = $this->post('minAmount');
                    $maxAmount = $this->post('maxAmount');
                    $creditFeesAmount = $this->post('creditFeesAmount');
                    $interestRate = $this->post('interestRate');
                    $durationInDays = $this->post('durationInDays');
                    //
                    $id = $this->products_model->insertProduct($code, $minAmount, $maxAmount, $creditFeesAmount, $interestRate, $durationInDays);
                    $this->response(
                        ['status' => TRUE, 'msg' => 'Produit Ajoute !', 'res' => array($id)], REST_Controller::HTTP_CREATED
                    );
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}