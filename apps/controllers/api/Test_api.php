<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include Rest Controller library 
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Test_api extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();

		// chargement du model associe
        $this->load->model('Users/users_model');  
	}

	public function tokenGen_post()
	{   
		$token_data['uid'] = "testapi2023";
		$token_data['appname'] = "appname"; 

		$tokenData = $this->authorization_token->generateToken($token_data);

		$final = array();
		$final['token'] = $tokenData;
		$final['status'] = 'ok';

		$this->response($final); 
	}

	public function verify_post()
	{  
		$headers = $this->input->request_headers(); 
		if (isset($headers['Authorization'])) {
			$decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
			$this->response($decodedToken);
		}
		else {
			$this->response(['Authentication failed'], REST_Controller::HTTP_OK);
		}		  
	}

	public function login_post()
	{
		$username = "username";
		$pwd = "password";
		// get user if exist
		$user = $this->users_model->findUser($username, $pwd);

		if(!empty($user)) {
			$atoken = "testLogin";
			$this->response(['status' => TRUE, 'msg' => 'Utilisateur trouve !', 'res' => $user, 'access' => $atoken], REST_Controller::HTTP_OK);
		} else $this->response(['status' => FALSE, 'msg' => 'Aucun utilisateur correspondant!', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);

	}
 
}

