<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include Rest Controller library 
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Credits extends REST_Controller {

	/**
	 * Api credits controller.
	*/

    function __construct() {
        // Constructeur de la classe
        parent::__construct(); 
        
        // chargement du model associe
        $this->load->model('Credits/credits_model');

        // definition des méthodes
        $this->methods['allCredits_get']['limit'] = 100;
        $this->methods['findCreditById_post']['limit'] = 20;
        $this->methods['findCreditByPhone_post']['limit'] = 20;
        $this->methods['insertCredit_post']['limit'] = 20;
    }

    public function allCredits_get() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $credits = $this->credits_model->getAllCredits();

                    (!empty($credits))?$this->response(['status' => TRUE, 'msg' => 'OK', 'res' => $credits], REST_Controller::HTTP_OK):
                    $this->response(['status' => FALSE, 'msg' => 'Aucun credit pris.', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function findCreditById_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $id = $this->post('id');

                    if(!empty($id)) $credit = $this->credits_model->getAllCredits($id);
                    else $this->response(['status' => FALSE, 'msg' => 'Id vide', 'res' => array()], REST_Controller::HTTP_BAD_REQUEST);

                    (!empty($credit))?$this->response(['status' => TRUE, 'msg' => 'OK', 'res' => $credit], REST_Controller::HTTP_OK):
                    $this->response(['status' => FALSE, 'msg' => 'Id non reconnu', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function findCreditByPhone_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $phone = $this->post('phone');
                    // check if $phone is a number and this number is orange
                    if(!empty($phone) && preg_match('/^[0-9]{10}+$/', $phone) && (substr($phone, 0, 2) === "07")){
                        $credits = $this->credits_model->getAllCredits(null, $phone);

                        if(empty($credits)) {
                            $this->response(['status' => FALSE, 'msg' => 'Aucune transaction associee', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
                        } else {
                            $now = new DateTime('now', new DateTimeZone('Africa/Abidjan'));
                            $dd = $now->format('Y-m-d H:i:s');

                            foreach($credits as $key => $value) { 
                                if($value->issueDate >= $dd){
                                    $this->set_response([
                                        'status' => TRUE, 
                                        'msg' => 'Vous avez un pret en cours!',
                                        'res' => $value
                                    ], REST_Controller::HTTP_OK);
                                }
                            }
                        }
                    } else {
                        $this->set_response(['status' => FALSE, 'msg' => 'Numero incorrecte !'], REST_Controller::HTTP_BAD_REQUEST);
                    }
                // } else {
                //     $this->response($decodedToken);
                // }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function insertCredit_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    // get parameters
                    $amount = $this->post('amount');
                    $idClient = $this->post('idClient');
                    $idProduct =  $this->post('idProduct');
                    $durationInDays = $this->post('durationInDays');

                    if(!empty($amount) && !empty($idClient) && !empty($idProduct) && !empty($durationInDays)) {
                        // check if amount is numeric
                        if(!is_numeric($amount))
                            $this->set_response(['status' => FALSE, 'msg' => 'Format montant incorrecte !'], REST_Controller::HTTP_BAD_REQUEST);
                        
                        // check value of amount (between min and  max amount for this product)
                        if(($amount >= $this->post('minAmount')) && ($amount<= $this->post('maxAmount'))){
                            // format result to send
                            $id = $this->credits_model->insertCredit($idClient, $idProduct, $amount, $durationInDays);
                            $this->response(
                                ['status' => TRUE, 'msg' => 'Credit ajoute !', 'res' => array($id)], REST_Controller::HTTP_CREATED
                            );
                        } else {
                            $this->set_response(['status' => FALSE, 'msg' => 'Le montant souhaite doit etre dans la fourchette du pack choisi.'], REST_Controller::HTTP_BAD_REQUEST);
                        }
                    } else $this->set_response(['status' => FALSE, 'msg' => 'Aucune valeur ne doit etre vide !'], REST_Controller::HTTP_BAD_REQUEST);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}