<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include Rest Controller library 
require(APPPATH.'libraries/REST_Controller.php');
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller {

	/**
	 * Api users controller.
	*/

    function __construct()
    {
        // Constructeur de la classe
        parent::__construct(); 
        
        // chargement du model associe
        $this->load->model('Users/users_model');

        // definition des méthodes
        $this->methods['allUsers_get']['limit'] = 100;
        $this->methods['findUser_post']['limit'] = 20;
        $this->methods['insertUser_post']['limit'] = 20;
    }

    public function allUsers_get() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $users = $this->users_model->getAllUsers();

                    (!empty($users))?$this->response(['status' => TRUE, 'msg' => 'OK', 'res' => $users], REST_Controller::HTTP_OK):
                    $this->response(['status' => FALSE, 'msg' => 'Aucun utilisateur trouve.', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function findUser_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    // get variables
                    $username = $this->post('username');
                    $pwd = $this->post('password');
                    // check if not empty
                    if(!empty($username) && !empty($pwd)) {
                        // get user if exist
                        $user = $this->users_model->findUser($username, $pwd);

                        if(!empty($user)) {
                            $atoken = $this->generateNumericOTP(8);
                            $this->response(['status' => TRUE, 'msg' => 'Utilisateur trouve !', 'res' => $user, 'access' => $atoken], REST_Controller::HTTP_OK);
                        } else $this->response(['status' => FALSE, 'msg' => 'Aucun utilisateur correspondant!', 'res' => array()], REST_Controller::HTTP_NOT_FOUND);
                    } else $this->set_response(['status' => FALSE, 'msg' => 'Aucune valeur ne doit etre vide !'], REST_Controller::HTTP_BAD_REQUEST);
                    
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
            
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function insertUser_post() {
        try {
            // if (isset($headers['Authorization'])) {
            //     $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            //     if ($decodedToken['status']) {
                    $this->users_model->insertUser();
                    $this->response(
                        ['status' => TRUE, 'msg' => 'Utilisateur ajoute !', 'res' => array()], REST_Controller::HTTP_CREATED
                    );
            //     } else {
            //         $this->response($decodedToken);
            //     }
            // } else {
            //     $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
            // }
        } catch (Exception $e) {
            //throw $th;
            $this->response(['status' => FALSE, 'msg' => 'Erreur : '.$e->getMessage()], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function generateNumericOTP($n) {
      
        // Take a generator string which consist
        $generator = "A1B3C5D7E9F0G2H4I6J8KMLNPOQTSVUXYWZ";
      
        $result = "";
      
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
      
        // Return result
        return $result;
    }

}