<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>API OBA TEST</title>

    <style>
        ::selection { background-color: #E13300; color: white; }
        ::-moz-selection { background-color: #E13300; color: white; }

        body {
            background-color: #FFF;
            margin: 40px;
            font: 16px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
            word-wrap: break-word;
        }

        a {
            color: #039;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 24px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 16px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body {
            margin: 0 15px 0 15px;
        }

        p.footer {
            text-align: right;
            font-size: 16px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;
        }
    </style>
</head>
<body>

<div id="container">
    <h1>API OBA TEST</h1>

    <div id="body">

        <p>
            Le projet entier est disponible sur Git : <br>
            <a href="https://gitlab.com/dgc2412/api-oba.git" target="_blank">
                https://gitlab.com/dgc2412/api-oba.git
            </a>
        </p>

        <p>
            Vous pouvez également tester les Apis directement en ligne : <br>
            <a href="https://api.postman.com/collections/3496512-9a7ca724-7f34-4cfa-bbd8-f691b604d0a4?access_key=PMAT-01GYGVXK9S9F00B6VMDNY4V0Z8" target="_blank">
            https://api.postman.com/collections/3496512-9a7ca724-7f34-4cfa-bbd8-f691b604d0a4?access_key=PMAT-01GYGVXK9S9F00B6VMDNY4V0Z8
            </a>
        </p>

        <p>
            Cliquez sur les liens suivants pour voir le rendu des APIs :
        </p>

        <ol>
            <li><a href="#" target="_blank">api/Token/reGenToken</a><br> - (POST) Generation d'un token, prend en paramètre le nom de l'application $appname.</li>
            <li><a href="<?php echo site_url('api/clients/allClients'); ?>" target="_blank">api/clients/allClients</a><br> - (GET) La liste des clients (demandeurs de pret).</li>
            <li><a href="#">api/clients/insertClient</a><br> - (POST) Insertion en base d'un client (demandeur de pret), prend en paramètre le numero $phone.</li>
            <li><a href="<?php echo site_url('api/credits/allCredits'); ?>" target="_blank">api/credits/allCredits</a><br> - (GET) La liste des prêts contractés.</li>
            <li><a href="#">api/credits/findCreditById</a><br> - (POST) Recherche des informations sur un prêt en particulier par l'Id du credit.</li>
            <li><a href="#">api/credits/findCreditByPhone</a><br> - (POST) Permet de savoir si le client (demandeur) n'a pas de prêt en cours, prend en paramètre le numero $phone.</li>
            <li><a href="#">api/credits/insertCredit</a><br> - (POST) Insertion en base d'une demande de prêt.</li>
            <li><a href="<?php echo site_url('api/logs/all'); ?>" target="_blank">api/logs/all</a><br> - (GET) Le log des actions utilisateurs.</li>
            <li><a href="#">api/logs/insertLog</a><br> - (POST) Insertion dans les logs d'une action.</li>
            <li><a href="<?php echo site_url('api/products/allProducts'); ?>" target="_blank">api/products/allProducts</a><br> - (GET) La  liste des produits enregistrés dans notre BD locale.</li>
            <li><a href="#">api/products/findProduct</a><br> - (POST) Verification de l'enregistrement du produit dans notre BD.</li>
            <li><a href="#">api/products/insertProduct</a><br> - (POST) Insertion dans un produit dans notre BD.</li>
            <li><a href="<?php echo site_url('api/users/allUsers'); ?>" target="_blank">api/users/allUsers</a><br> - (GET) La liste des utilisateurs.</li>
            <li><a href="#">api/users/findUser</a><br> - (POST) Recherche d'un utilisateur avec le $username & le $password (pour la connexion).</li>
            <li><a href="#">api/users/insertUser</a><br> - (POST) Creation d'un utilisateur.</li>
        </ol>

    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>'.CI_VERSION.'</strong>' : '' ?></p>
</div>

<script src="https://code.jquery.com/jquery-1.12.0.js"></script>

<script>
    // Create an 'App' namespace
    var App = App || {};

    // Basic rest module using an IIFE as a way of enclosing private variables
    App.rest = (function restModule(window) {
        // Fields
        var _alert = window.alert;
        var _JSON = window.JSON;

        // Cache the jQuery selector
        var _$ajax = null;

        // Cache the jQuery object
        var $ = null;

        // Methods (private)

        /**
         * Called on Ajax done
         *
         * @return {undefined}
         */
        function _ajaxDone(data) {
            // The 'data' parameter is an array of objects that can be iterated over
            _alert(_JSON.stringify(data, null, 2));
        }

        /**
         * Called on Ajax fail
         *
         * @return {undefined}
         */
        function _ajaxFail() {
            _alert('Oh no! A problem with the Ajax request!');
        }

        /**
         * On Ajax request
         *
         * @param {jQuery} $element Current element selected
         * @return {undefined}
         */
        function _ajaxEvent($element) {
            $.ajax({
                    // URL from the link that was 'clicked' on
                    url: $element.attr('href')
                })
                .done(_ajaxDone)
                .fail(_ajaxFail);
        }

        /**
         * Bind events
         *
         * @return {undefined}
         */
        function _bindEvents() {
            // Namespace the 'click' event
            _$ajax.on('click.app.rest.module', function (event) {
                event.preventDefault();

                // Pass this to the Ajax event function
                _ajaxEvent($(this));
            });
        }

        /**
         * Cache the DOM node(s)
         *
         * @return {undefined}
         */
        function _cacheDom() {
            _$ajax = $('#ajax');
        }

        // Public API
        return {
            /**
             * Initialise the following module
             *
             * @param {object} jQuery Reference to jQuery
             * @return {undefined}
             */
            init: function init(jQuery) {
                $ = jQuery;

                // Cache the DOM and bind event(s)
                _cacheDom();
                _bindEvents();
            }
        };
    }(window));

    // DOM ready event
    $(function domReady($) {
        // Initialise the App module
        App.rest.init($);
    });
</script>

</body>
</html>
