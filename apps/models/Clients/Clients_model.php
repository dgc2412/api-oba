<?php

class Clients_model extends CI_Model {
    public $phone;
    public $creationDate;
    public $modificationDate;
    public $status;
    public $now;

    function __construct() {
        // Constructeur de la classe
        parent::__construct(); 
        
        // date
        $this->now = new DateTime('now', new DateTimeZone('Africa/Abidjan'));
    }

    public function getAllClients($phone=null) {
        if($phone != null) $this->db->where('phone', $phone);
        $query = $this->db->get('clients');
        return $query->result();
    }

    public function insertClient($phone) {
        $this->phone            = $phone;
        $this->status           = 1;
        $this->creationDate     = $this->now->format('Y-m-d H:i:s');
        $this->modificationDate = $this->now->format('Y-m-d H:i:s');

        $this->db->insert('clients', $this);

        return  $this->db->insert_id();
    }

    public function updateClient() {
        $this->phone            = $_POST['phone'];
        $this->status           = $_POST['status'];
        $this->modificationDate = $this->now->format('Y-m-d H:i:s');

        $this->db->update('clients', $this, array('id' => $_POST['id']));
    }
}