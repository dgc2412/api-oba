<?php

class Products_model extends CI_Model {
    public $code;
    public $minAmount;
    public $maxAmount;
    public $creditFeesAmount;
    public $interestRate;
    public $durationInDays;

    public function getAllProducts() {
        $query = $this->db->get('products');
        return $query->result();
    }

    public function findProduct($code) {
        $query = $this->db->get_where('products', array('code' => $code));
        return $query->result();
    }

    public function insertProduct($code, $minAmount, $maxAmount, $creditFeesAmount, $interestRate, $durationInDays) {
        $this->code                     = $code;
        $this->minAmount                = $minAmount;
        $this->maxAmount                = $maxAmount;
        $this->creditFeesAmount         = $creditFeesAmount;
        $this->interestRate             = $interestRate;
        $this->durationInDays           = $durationInDays;

        $this->db->insert('products', $this);

        return  $this->db->insert_id();
    }
}