<?php

class Users_model extends CI_Model {
    public $username;
    public $password;
    public $role;
    public $creationDate;
    public $modificationDate;
    public $status;
    public $now;

    function __construct() {
        // Constructeur de la classe
        parent::__construct();

        //
        $this->load->library('encryption');
        // date
        $this->now = new DateTime('now', new DateTimeZone('Africa/Abidjan'));
    }

    public function getAllUsers()
    {
        $query = $this->db->get('users');
        return $query->result();
    }

    public function findUser($username, $password)
    {
        $query = $this->db->get_where('users', array('username' => $username, 'password' =>  sha1($password)));
        return $query->result();
    }

    public function insertUser()
    {
        $this->username             = $_POST['username'];
        $this->role                 = $_POST['role'];
        $this->password             = sha1($_POST['password']);
        $this->creationDate         = $this->now->format('Y-m-d H:i:s');
        $this->modificationDate     = $this->now->format('Y-m-d H:i:s');
        $this->status               = 1;

        $this->db->insert('users', $this);
    }

    public function updateUser()
    {
        $this->username             = $_POST['username'];
        $this->password             = sha1($_POST['password']);
        $this->modificationDate     = $this->now->format('Y-m-d H:i:s');
        $this->status               = $_POST['status'];

        $this->db->update('users', $this, array('id' => $_POST['id']));
    }
}