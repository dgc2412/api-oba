<?php

class Credits_model extends CI_Model {
    public $idClient;
    public $idProduct;
    public $amount;
    public $creationDate;
    public $modificationDate;
    public $issueDate;
    public $status;
    public $now;

    function __construct() {
        // Constructeur de la classe
        parent::__construct(); 
        
        // date
        $this->now = new DateTime('now', new DateTimeZone('Africa/Abidjan'));
    }

    public function getAllCredits($id=null, $phone=null) {
        if($id != null) $this->db->where('id', $id);
        if($phone != null) {
            $this->db->where('phoneClient', $phone);
        }
        $query = $this->db->get('v_credits');
        return $query->result();
    }

    public function insertCredit($idClient, $idProduct, $amount, $durationInDays) {
        $this->idClient             = $idClient;
        $this->idProduct            = $idProduct;
        $this->amount               = $amount;
        $this->creationDate         = $this->now->format('Y-m-d H:i:s');
        $this->modificationDate     = $this->now->format('Y-m-d H:i:s');

        $interval = 'P'.$durationInDays.'D';
        $date = new DateTime($this->now->format('Y-m-d H:i:s')); // Y-m-d
        $date->add(new DateInterval($interval));
        
        $this->issueDate            = $date->format('Y-m-d H:i:s');
        $this->status               = 1;

        $this->db->insert('credits', $this);

        return  $this->db->insert_id();
    }

    public function updateCredit() {
        $this->idClient             = $_POST['idClient'];
        $this->idProduct            = $_POST['idProduct'];
        $this->amount               = $_POST['amount'];
        $this->modificationDate     = $this->now->format('Y-m-d H:i:s');
        $this->issueDate            = $_POST['issueDate'];
        $this->status               = $_POST['status'];

        $this->db->update('credits', $this, array('id' => $_POST['id']));
    }
}