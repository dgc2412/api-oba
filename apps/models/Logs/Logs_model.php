<?php

class Logs_model extends CI_Model {
    public $idUser;
    public $action;
    public $creationDate;
    public $now;

    function __construct() {
        // Constructeur de la classe
        parent::__construct(); 
        
        // date
        $this->now = new DateTime('now', new DateTimeZone('Africa/Abidjan'));
    }

    public function getAll() {
        $query = $this->db->get('logs');
        return $query->result();
    }

    public function insertLog($idUser, $action) {
        $this->idUser            = $idUser;
        $this->action            = $action;
        $this->creationDate      = $this->now->format('Y-m-d H:i:s');

        $this->db->insert('logs', $this);
    }
}