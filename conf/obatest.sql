-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : Dim 23 avr. 2023 à 10:56
-- Version du serveur :  5.7.41-cll-lve
-- Version de PHP : 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `obatest`
--

-- --------------------------------------------------------

--
-- Structure de la table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `phone` varchar(13) NOT NULL COMMENT 'client number phone',
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'added client date',
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'updated client date',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'status of client'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `credits`
--

CREATE TABLE `credits` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL COMMENT 'id client column',
  `idProduct` int(11) NOT NULL COMMENT 'id product column',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT 'requested amount by client',
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issueDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `minAmount` int(11) NOT NULL DEFAULT '0',
  `maxAmount` int(11) NOT NULL DEFAULT '0',
  `creditFeesAmount` int(11) NOT NULL DEFAULT '0',
  `interestRate` float NOT NULL DEFAULT '0',
  `durationInDays` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `code`, `minAmount`, `maxAmount`, `creditFeesAmount`, `interestRate`, `durationInDays`) VALUES
(1, 'picoCredit', 5000, 100000, 500, 1.8, 30);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL COMMENT 'username',
  `password` varchar(255) NOT NULL COMMENT 'password of user',
  `role` enum('admin','user','','') NOT NULL,
  `creationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'status of user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `creationDate`, `modificationDate`, `status`) VALUES
(1, 'admin', '371C685F40AE9A30C566F62ECDB5F39E028C5CCF', 'admin', '2023-04-21 02:44:20', '2023-04-21 02:44:20', 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_credits`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `v_credits` (
`id` int(11)
,`phoneClient` varchar(13)
,`codeProduct` varchar(255)
,`requestedAmount` int(11)
,`interestRate` float
,`creditFeesAmount` int(11)
,`dueAmount` double(19,2)
,`creationDate` datetime
,`durationInDays` int(11)
,`issueDate` datetime
,`status` tinyint(1)
);

-- --------------------------------------------------------

--
-- Structure de la vue `v_credits`
--
DROP TABLE IF EXISTS `v_credits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cpses_vh8d79nbi5`@`localhost` SQL SECURITY DEFINER VIEW `v_credits`  AS SELECT `c`.`id` AS `id`, `clt`.`phone` AS `phoneClient`, `p`.`code` AS `codeProduct`, `c`.`amount` AS `requestedAmount`, `p`.`interestRate` AS `interestRate`, `p`.`creditFeesAmount` AS `creditFeesAmount`, round(((`c`.`amount` * (1 + (`p`.`interestRate` / 100))) + `p`.`creditFeesAmount`),2) AS `dueAmount`, `c`.`creationDate` AS `creationDate`, `p`.`durationInDays` AS `durationInDays`, `c`.`issueDate` AS `issueDate`, `c`.`status` AS `status` FROM ((`clients` `clt` join `credits` `c`) join `products` `p`) WHERE ((`c`.`idClient` = `clt`.`id`) AND (`c`.`idProduct` = `p`.`id`)) ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- Index pour la table `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idClient` (`idClient`),
  ADD KEY `idProduct` (`idProduct`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUser` (`idUser`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `password` (`password`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `credits`
--
ALTER TABLE `credits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `credits`
--
ALTER TABLE `credits`
  ADD CONSTRAINT `credits_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
