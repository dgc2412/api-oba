CREATE VIEW v_credits
AS
SELECT
    c.id,
    clt.phone as phoneClient,
    p.code as codeProduct,
    c.amount as requestedAmount,
    p.interestRate,
    p.creditFeesAmount,
    ROUND((c.amount * (1 + p.interestRate/100) + p.creditFeesAmount), 2) AS dueAmount,
    c.creationDate,
    p.durationInDays,
    c.issueDate,
    c.status
FROM
    clients as clt,
    credits as c,
    products as p

WHERE 
    c.idClient  = clt.id and 
    c.idProduct = p.id
    